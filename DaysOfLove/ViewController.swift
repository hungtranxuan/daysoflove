//
//  ViewController.swift
//  DaysOfLove
//
//  Created by Robert Travis on 3/20/19.
//  Copyright © 2019 Hung Tran Xuan. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKUIDelegate {
    
    @IBOutlet var playVideo: WKWebView!
    
//    override func loadView() {
//        let webConfiguration = WKWebViewConfiguration()
//        webConfiguration.allowsInlineMediaPlayback = true
//        playVideo = WKWebView(frame: .zero, configuration: webConfiguration)
//        playVideo.uiDelegate = self
//        view = playVideo
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let myEmbeddedURL = URL(string: "https://www.youtube.com/embed/-jayKKkPegk")//embedded video unavailable to play
        //let myRequest = URLRequest(url: myEmbeddedURL!)
        
        let myURL = URL(string: "https://www.youtube.com/watch?v=-jayKKkPegk")
        let youtubeRequest = URLRequest(url: myURL!)
        
        playVideo.load(youtubeRequest)
        
        //This line causes small video not fit for the frame
        //playVideo.loadHTMLString("<iframe width=\"\(playVideo.frame.width)\" height=\"\(playVideo.frame.height)\" src=\"https://www.youtube.com/embed/-jayKKkPegk?&playinline=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>", baseURL: nil)
        
    }
    
}

